package motscomposes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import de.danielnaber.jwordsplitter.AbstractWordSplitter;
import de.danielnaber.jwordsplitter.GermanWordSplitter;
import fichier.FichierW;


public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		
		
		// l'utilisateur entre un énoncé ou décide d'ajouter un nouveau mot
		Scanner scanner = new Scanner( System.in );
		System.out.println("Entrer un énoncer en allemand (taper 'add' pour ajouter un nouveau mot composé au dictionnaire).");
		String enonce = scanner.nextLine();
		
		/////////// AJOUTER UN NOUVEAU MOT ///////////
		
		if (enonce.equals("add")) {
			System.out.println("Entrer le mot à ajouter (taper 'x' pour quitter sans ajouter de mot).");
			String r = scanner.nextLine();
			
			// SI r = "x", on quitte le programme sans ajouter de mot
			if (r.contentEquals("x")) {
				return;
				
			// SINON on split ce mot et on l'ajoute au lexique (différent du lex de base)
			} else {
				AbstractWordSplitter splitter = new GermanWordSplitter(true);
				List<String> parts = splitter.splitWord(r);
				//System.out.println(parts);
				
				FichierW w = new FichierW("LexiquePersonnalise.tsv");

				/* format du fichier en sortie :
				 * Motcomposé		lemme1 lemme2 lemme3 etc
				 * Arbeitsmarkt		Arbeit markt
				*/
				w.ajout("\n" + r + "\t" + String.join(" ",parts));
				w.fermer();
				
			}
		}
		
		/////////// TRAITEMENT DE L'ENONCE ///////////
		
		else {
		
			StringTokenizer st;
			ArrayList colonne = new ArrayList();
			ArrayList<String> tabenonce = new ArrayList();
			ArrayList<String> tabOP = new ArrayList<String>();
			Hashtable<String,String> mc = null;
			
			try {	// ouvrir un fichier
				
				
	
	            File f = new File("gecodb_v01.01.tsv");
	            BufferedReader b = new BufferedReader(new FileReader(f));
	
	            String readLine = "";
	
	            System.out.println("Reading file using Buffered Reader");
	            

	
	            while ((readLine = b.readLine()) != null) {		// parcours du fichier

	    			
	    			ArrayList elmts = new ArrayList();
	            	
	                //System.out.println(readLine);
	            	
	            	// split aux tabulations et on ajoute la ligne dans colonne
	            		// (la méthode split("\t") ne fonctionne pas sur les fichiers .tsv)
	            	st = new StringTokenizer(readLine, "\t");
	            	
	            	while((st.hasMoreElements()) && (colonne.size() != 2)){
	                    colonne.add(st.nextElement().toString());
	                    //System.out.println(colonne);
	            	}
	            	
	            	if (colonne.size() == 2) {
	            	

	
	            		// prend la 1ère colonne qu'on stocke dans motsplit
	    					// ex. motsplit = "Zeit_Punkt"
	            		String motsplit = (String) colonne.get(0);
	    			
	            		// split motsplit au caractère séparateur
	            			// ex. elmts = ["Zeit", "Punkt"]
	            		elmts = new ArrayList<>(Arrays.asList(motsplit.split("_")));
	    			
	            		// on instancie un nouvel objet de type Lexique
	            		Lexique t1 = new Lexique(elmts);
	    			
	            		// appel de la méthode pour recomposer les mots du fichier
	            		mc = t1.joinmotscompo(elmts);	// dictionnaire avec comme clé le mot composé et comme valeur le mot décomposé
	            		//System.out.println(mc);		// 1 nouveau dict pour chaque ligne
	    			
	            		
		        		
	    			
	            	}
	    			
	    			colonne.clear();
	    			
	    			

	    			
		        	/////////// REPERER LES MOTS COMPOSES DANS UN ENONCE ///////////
	    			
		            tabenonce = new ArrayList<>(Arrays.asList(enonce.split(" ")));
		            
	            	for (String m:tabenonce) {
	            		
	            		if (mc.containsKey(m)) {
	            			//System.out.println(m);
	            			tabOP.add(mc.get(m));
	            		}
	            	 
	    			} // parc eno
	    			
	            } // fin parcours du fichier
	            	
	            

	            

			
// Wiedererkennungswert Mehrzweckhalle Einkommensgruppe Finanzmanagement
	            	

            
	            //System.out.println(tabOP);
	            //System.out.println(tabenonce);
	            System.out.println("\n#Votre énoncé :");
	            System.out.println(enonce);
	
	            
	            /////////// CORENLP POSTAGGING ///////////
	            
	    		
	            
	    		Nlp parsing = new Nlp(enonce);
	    		Hashtable ht = new Hashtable();
	    		
	    		ht = parsing.postagging(enonce);	// effectue le postagging
	    		
	    		
	    		System.out.println("\n#Analyse de l'énoncé :");
	    		
	    		// print chaque éléments de l'énoncé avec leur POS
	    		ht.forEach( (token, pos)
	    				-> System.out.println( token + " : " + pos) );
	    		
	    		System.out.println("\n#Eléments composés présents :");
	    		
	    		for (String e:tabOP) {
	    			System.out.println(e);
	    		}
	    		
	    		System.out.println("\n#Analyse des composants :");
	    		for (String e:tabOP) {
	    			Nlp e1 = new Nlp(e);
	    			Hashtable ht1 = e1.postagging(e);
	    			ht1.forEach( (token, pos)
		    				-> System.out.println( token + " : " + pos) );
	    		}
	            
				
			} //try
	         catch (IOException e) {
	        }
	
		} //else
		
		

	} //main
}
