package motscomposes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;


public class Lexique {
	ArrayList lex = new ArrayList();
	
	public Lexique(ArrayList l) {
		lex = l;
	}
		
			// BASE DE REGLES
	public Hashtable <String,String> joinmotscompo(ArrayList<String> t) {
		ArrayList elmts = new ArrayList();
		elmts = t;
		Hashtable<String,String> test = new Hashtable<String,String>();
		String join, mot = "", lemmes = "";
		
		try {
			
		
		
			// parcours chaque éléments du tableau
			for (int i=0; i< elmts.size(); i++) {
				String e = (String) elmts.get(i);	// e = élément qu'on parcourt actuellement
				
				// Prendre seulement les lemmes qui constituent le mot
					// ex. elmts = ["suchen", "#en", "Maschine"] => lemmes = ["suchen","Maschine"]
				String symboles = "-+=#()";
				char e0 = e.charAt(0);
				if (symboles.indexOf(e0) == -1) {		// s'il l'élmt ne contient pas de symboles servant à modifier le lemme
					lemmes = lemmes + " " +e;			// on l'ajoute dans la chaine de lemmes
				}

				
				//1. Ajout trait d'union
					// lorsqu'on rencontre "--"
					// ex. elmts = ["Irak", "--", "Krieg"] devient ["Irak", "-", "Krieg"] 
				if (e == "--") {
					elmts.set(i, "-");
				}
				
				//2. Supprimer terminaison
				//lorsqu'on rencontre '#' ou '-'
				//ex. elmts = ["suchen", "#en", "Maschine"] devient ["such", "Maschine"]
				else if ( (e.charAt(0) == '#') || (e.charAt(0) == '-') ) {
					String eprec = (String) elmts.get(i-1);
					int m = eprec.length();
					int n = e.length()-1;
					try {
						eprec = eprec.substring(0, m -n ); 	// eprec = eprec, mais on retire les n (longueur de e moins le caractère '#' ou '-') dernières lettres
					
						elmts.set(i-1, eprec);
						elmts.set(i, "");	// on le retire de la liste (j'utilise pas remove() pour ne pas changer la longueur)
					}
					catch (Exception x){
					}
				}
				
				// 3. Ajouter lettre
				//lorsqu'on rencontre "(...)"
				// ex. elmts = ["Reptil", "(i)", "+en", "Art"] devient ["Reptili", "+en", "Art"]
				else if ((e.charAt(0) == '(') && (i > 0)) {
					String eprec = (String) elmts.get(i-1);
					
					eprec = eprec + e.substring(1, e.length()-1);	// on ajoute le car entre parenthèses à l'élmt précédent
					// **??**
					elmts.set(i-1, eprec);
					elmts.set(i, "");
				}
				
				// 4. Ajouter des lettres + umlaut (tréma allemand : ä, ü et ö)
				//lorsqu'on rencontre "+="
				// ex. elmts = ["Wort", "+=er", "Buch"] devient ["Wörter", "", "Buch"]
				else if (e.charAt(0) == '+') {
					String eprec = (String) elmts.get(i-1);
					
					if (e.charAt(1) == '=') {
						//parcours elmt précédent à partir de la dernière lettre
						int u=eprec.length()-1;
						boolean umlaut = false;
						StringBuilder myString = new StringBuilder(eprec);
						
						// tant qu'on a pas ajouté de umlaut
						while ((umlaut == false) && (u > 0)) {
							// on parcourt la chaine du mot précédent
							switch (myString.charAt(u)) {
							case 'a' :		// remplacer 'a' par 'ä'
								myString.setCharAt(u, 'ä');
								umlaut = true;
								break;
							case 'u' :		// remplacer 'u' par 'ü'
								myString.setCharAt(u, 'ü');
								umlaut = true;
								break;
							case 'o' :		// remplacer 'o' par 'ö'
								myString.setCharAt(u, 'ö');
								umlaut = true;
								break;
							default :
								u--;	//si on ne croise pas 'a', 'u' ou 'o' on passe à la lettre précédente
							}
						}
						
						eprec = myString.toString();
						StringBuilder e1 = new StringBuilder(e);
						e1.deleteCharAt(1);		// supprime '='
						e = e1.toString();
					}
						eprec = eprec + e.substring(1, e.length());
						elmts.set(i-1, eprec);
						elmts.set(i, "");
				}
				
			} // fin parcours elmts
			
				join = String.join("",elmts);	// on rejoint tous les éléments
				String x = join.substring(1);	// x = toutes les lettres du mot sauf la première ex. LandesRegierung => x= "andesRegierung"
				char z = join.charAt(0);	// prend la première lettre du mot
				String lastelmt = (String) elmts.get(elmts.size()-1);
				if ( lastelmt != lastelmt.toLowerCase() ) {		// si la tête du mot est un nom (= commence par une majuscule)
					mot = Character.toUpperCase(z) + x.toLowerCase();		// met la première lettre du mot en majuscule + le reste des lettres passées en minuscules => Landesregierung
				}
				else {
					mot = join.toLowerCase();
				}
				test.put(mot,lemmes);	// clé : mot, valeur : lemmes qui le composent

			
		
		
		} catch (Exception x) {	
		}
		return test;
		}



	
}
