package motscomposes;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class Nlp {

	String nlp;
	
	public Nlp(String n) {
		nlp = n;
	}
	
	public Hashtable <CoreLabel,String> postagging(String p) {
		Hashtable <CoreLabel,String> tokenpos = new Hashtable <CoreLabel,String>();
		
		// création d'un objet Properties
		Properties props = new Properties();
		// définition du pipeline
		props.setProperty("annotators", "tokenize, ssplit, pos");
		// paramétrage pour l'allemand
		props.setProperty("props", "StanfordCoreNLP-german.properties");
		props.setProperty("tokenize.language","de");
		props.setProperty("tokenize.postProcessor", "edu.stanford.nlp.international.german.process.GermanTokenizerPostProcessor");
		props.setProperty("pos.model","edu/stanford/nlp/models/pos-tagger/german/german-hgc.tagger");
		props.setProperty("tokenize.verbose","false"); // True = affiche les tokens

		// création du pipeline
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		
		// cr�er une annotation vide avec le texte
		Annotation document = new Annotation(p);
		// lance l'annotation sur le texte
		pipeline.annotate(document);
		
		// Obtenir la liste des phrases
		List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
		for (CoreMap sentence : sentences) {

			// traitement des tokens
			for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
				String pos = token.get(PartOfSpeechAnnotation.class);
				tokenpos.put(token, pos);
			}
		}
		
		return tokenpos;	// retourne un tableau avec chaque token associé à son POS
	}

}
