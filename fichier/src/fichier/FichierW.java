package fichier;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class FichierW {
	private PrintWriter pw;
	
	//constructeur
	public FichierW(String nom) {
		
		try {
			pw = new PrintWriter(
					new BufferedWriter(
							new FileWriter(nom, true)));
			// le fichier est pr�t pour l'�criture (ouverture)
		} catch (IOException e) {
			System.out.println("Impossible d'ouvrir le fichier "+nom);
		}
	}
	
	public void fermer() {
		pw.close();
	}
	
	public boolean ecrire(String ligne) {
		try {
			pw.println(ligne);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public void ajout(String ligne) {
		pw.write(ligne);
	}
}
