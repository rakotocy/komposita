package fichier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

public class FichierR {

	private String nom;
	private BufferedReader dis;
	public int cptlignes;
	public int cptcar;
	public int nbvoy = 0;
	public ArrayList<String> tablignes;
	static Hashtable<Character,Integer> v;
	
	public FichierR(String n) {
		nom = n;
		cptlignes=0;
		cptcar=0;
		tablignes = new ArrayList<String>();
		voyelles();		 // chargement des voyelles
		try {
			dis = new BufferedReader(
					new FileReader(
							new File(n)));
			String ligne;
			try {
				while ((ligne = dis.readLine()) != null) {
					cptcar+=ligne.length();
					cptlignes++;
					tablignes.add(ligne);
					// parcours de la ligne car � car
					// comptage nb voy
					for (int i=0; i<ligne.length();i++) {
						if (v.containsKey(ligne.charAt(i))) {
							nbvoy++;
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} 
	}
	
		
	public String ligne(int i) {
		if (i<=cptlignes) {
			return tablignes.get(i-1);
		}else {
			return "";
		}
	}
	
	public String getnom() {
		return nom;
	}
	
	static void voyelles() {
		v = new Hashtable<Character,Integer>();
		v.put('a', 0);
		v.put('e', 0);
		v.put('i', 0);
		v.put('o', 0);
		v.put('u', 0);
		v.put('y', 0);
		v.put('é', 0);
		v.put('è', 0);
		v.put('ù', 0);
		v.put('à', 0);
		v.put('ï', 0);
	}
	

	
	
}


