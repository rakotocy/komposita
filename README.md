# Komposita

Projet JAVA permettant de découper automatiquement les mots composés allemands et d'obtenir leur POS

## Utilisation
- Lancer le fichier motscomposes/src/motscomposes/Main.java
- Entrer un énoncer en allemand
- Le texte entré va être analysé
- Possibilité d'ajouter de nouveaux mots en entrant "add"

Plus d'infos sur le projet : https://wiki.lezinter.net/_/Projets:Mots_compos%C3%A9s_en_allemand
